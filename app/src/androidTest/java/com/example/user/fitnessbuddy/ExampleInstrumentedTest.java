package com.example.user.fitnessbuddy;

import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<RegisterActivity> mRegisterActivityTestRule = new ActivityTestRule<RegisterActivity>(RegisterActivity.class);

    private IntentServiceIdlingResource idlingResource;
    @Before
    public void registerIntentServiceIdlingResource(){
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        idlingResource = new IntentServiceIdlingResource(instrumentation.getTargetContext());
        Espresso.registerIdlingResources(idlingResource);
    }

    @After
    public void unregisterIntentServiceIdlingResource(){
        Espresso.unregisterIdlingResources(idlingResource);
    }

    @Test
    public void createAccountTest() throws InterruptedException {
        onView(withId(R.id.emailGet)).perform(click()).perform(typeText("finalTest10@espresso.com"));
        onView(withId(R.id.passwordGet)).perform(click()).perform(typeText("12345678"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.signUpButton)).perform(click());

        Thread.sleep(6000);
        onView(withId(R.id.nameGet)).perform(click()).perform(typeText("Ivan"));
        onView(withId(R.id.surnameGet)).perform(click()).perform(typeText("Espresso"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.weightGet)).perform(click()).perform(typeText(String.valueOf("15")));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.heightGet)).perform(click()).perform(typeText(String.valueOf("150")));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.bioGet)).perform(click()).perform(typeText("I am espresso and I create account automatically"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.saveInfo)).perform(click());

        Thread.sleep(4000);
        onView(withId(R.id.viewpager)).perform(swipeLeft());
        onView(withId(R.id.viewpager)).perform(swipeLeft());
        onView(withId(R.id.viewpager)).perform(swipeRight());
        onView(withId(R.id.toolbar)).perform(swipeUp());


        onView(withId(R.id.new_activity_button)).perform(click());
        Thread.sleep(6000);

        onView(withId(R.id.startRunBtn)).perform(click());
        onView(withId(R.id.runName)).perform(click()).perform(typeText("EspressoRun"));
//        Espresso.closeSoftKeyboard();
        onView(withId(R.id.runFeel)).perform(click()).perform(typeText("Feeling EspressoPower"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.submitBtn)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.endRunBtn)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.viewpager)).perform(swipeLeft());
        Thread.sleep(1000);
        onView(withId(R.id.profileSettings)).perform(click());
        Thread.sleep(1000);
        onView(withId(R.id.nameNew)).perform(click()).perform(typeText("Espresso"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.surnameNew)).perform(click()).perform(typeText("Espresso"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.weightNew)).perform(click()).perform(typeText("150"));
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.biographyNew)).perform(click()).perform(typeText("Espresso"));
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.nameChange)).perform(click());
        onView(withId(R.id.surnameChange)).perform(click());
        onView(withId(R.id.weightChange)).perform(click());
        onView(withId(R.id.biographyChange)).perform(click());
        onView(withId(R.id.deleteAcc)).perform(click());
    }



}



