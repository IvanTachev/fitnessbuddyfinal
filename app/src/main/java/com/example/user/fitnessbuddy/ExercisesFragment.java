package com.example.user.fitnessbuddy;


import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.LocationCallback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.R.attr.key;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExercisesFragment extends Fragment implements View.OnClickListener {

    public Running running;
    private LinearLayout dynamicLayout;
    private TextView totalDistanceShow;
    private TextView moodShow;
    private TextView speedShow;
    private LinearLayout exerciseLayout;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private GeoFire geoFire;
    private DatabaseReference myRef;
    private ImageView mapsView;
    private Location myStartingPoint;
    private Location myFinishingPoint;
    private double firstMarkerLatitude;
    private double firstMarkerLongtitude;
    private double secondMarkerLatitude;
    private double secondMarkerLongtitude;
    private Button trackBtn;
    private String imageFileURL;
    private int testTd;
    private String testName;
    private String testMood;
    private int testAvgSpeed;


    public ExercisesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_exercises, container, false);

        firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = firebaseAuth.getCurrentUser();
        myRef = FirebaseDatabase.getInstance().getReference("users/" + user.getUid() + "/Runs");
        firebaseDatabase = FirebaseDatabase.getInstance();
        final GeoFire geoFire = new GeoFire(myRef);
        mapsView = (ImageView) v.findViewById(R.id.mapsView);
        dynamicLayout = (LinearLayout) v.findViewById(R.id.dynamicLayout);
        exerciseLayout = (LinearLayout) v.findViewById(R.id.activity_exercises);
        totalDistanceShow = (TextView) v.findViewById(R.id.totalDistanceShow);
        moodShow = (TextView) v.findViewById(R.id.moodShow);
        speedShow = (TextView) v.findViewById(R.id.avgSpeedShow);




        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null){
                    dynamicLayout.setVisibility(RelativeLayout.INVISIBLE);
                }else{
                    running = dataSnapshot.getValue(Running.class);
                    testName = running.getName();
                    testTd = running.getTotalDistance();
                    testMood = running.getMood();
                    testAvgSpeed = running.getAverageSpeed();
                    geoFire.getLocation(testName + "Start", new LocationCallback() {
                        @Override
                        public void onLocationResult(String key, final GeoLocation location) {

                            geoFire.getLocation(testName + "Finish", new LocationCallback() {
                                @Override
                                public void onLocationResult(String key, GeoLocation loc) {
                                    if (location != null){
                                        if (loc != null){
                                            firstMarkerLatitude = location.latitude;
                                            firstMarkerLongtitude = location.longitude;
                                            secondMarkerLatitude = loc.latitude;
                                            secondMarkerLongtitude = loc.longitude;
                                            imageFileURL = "https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=400x400&maptype=road&markers=color:green%7Clabel:S%7C" + firstMarkerLatitude + "," + firstMarkerLongtitude + "&markers=color:red%7Clabel:F%7C" + secondMarkerLatitude + "," + secondMarkerLongtitude + "&key=AIzaSyBeVVLM9QyWmgntMdQoKlYDRfItV8h8Qts";
                                            new ImageDownload(imageFileURL, mapsView).execute();
                                            dynamicLayout.setVisibility(RelativeLayout.VISIBLE);
                                            dynamicLayout.setElevation(2);
                                            totalDistanceShow.setText(Integer.toString(testTd));
                                            moodShow.setText(testMood);
                                            speedShow.setText(Integer.toString(testAvgSpeed));



                                        }
                                    }
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            System.out.println(String.format("There is no location for key %s in GeoFire", key));
                        }
                    });}
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        geoFire.getLocation(testName + "Start", new LocationCallback() {
            @Override
            public void onLocationResult(String key, final GeoLocation location) {

                geoFire.getLocation(testName + "Finish", new LocationCallback() {
                    @Override
                    public void onLocationResult(String key, GeoLocation loc) {
                        if (location != null){
                            if (loc != null){
                                firstMarkerLatitude = location.latitude;
                                firstMarkerLongtitude = location.longitude;
                                secondMarkerLatitude = loc.latitude;
                                secondMarkerLongtitude = loc.longitude;
                                imageFileURL = "https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=200x200&maptype=road&markers=color:green%7Clabel:S%7C" + firstMarkerLatitude + "," + firstMarkerLongtitude + "&markers=color:red%7Clabel:F%7C" + secondMarkerLatitude + "," + secondMarkerLongtitude + "&key=AIzaSyBeVVLM9QyWmgntMdQoKlYDRfItV8h8Qts";
                                new ImageDownload(imageFileURL, mapsView).execute();


                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println(String.format("There is no location for key %s in GeoFire", key));
            }
        });
        Button trackBtn = (Button) v.findViewById(R.id.new_activity_button);
        trackBtn.setOnClickListener(this);


        return v;
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.new_activity_button:
                startActivity(new Intent(getActivity(), MapsActivity.class));
                break;

        }
    }
}