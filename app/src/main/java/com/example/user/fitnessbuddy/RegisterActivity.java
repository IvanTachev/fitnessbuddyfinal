package com.example.user.fitnessbuddy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;


public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText nameText;
    private EditText surnameText;
    private EditText emailText;
    private EditText passwordText;
    //private EditText passwordRepeat;
    private Button mSignUpBtn;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);


        nameText = (EditText) findViewById(R.id.nameGet);
        surnameText = (EditText) findViewById(R.id.surnameGet);
        emailText = (EditText) findViewById(R.id.emailGet);
        passwordText = (EditText) findViewById(R.id.passwordGet);
        //passwordRepeat = (EditText) findViewById(R.id.passwordRepeat);
        mSignUpBtn = (Button) findViewById(R.id.signUpButton);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        mSignUpBtn.setOnClickListener(this);


    }





    private void signUp(){
        //String aName = nameText.getText().toString();
        //String aSurname = surnameText.getText().toString();
        String aEmail = emailText.getText().toString().trim();
        String aPassword = passwordText.getText().toString().trim();

        if (TextUtils.isEmpty(aEmail)){
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(aPassword)){
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }




        firebaseAuth.createUserWithEmailAndPassword(aEmail, aPassword)
        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    finish();
                    Toast.makeText(RegisterActivity.this, "Registered succesfully", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), InfoActivity.class));
                } else {
                    Toast.makeText(RegisterActivity.this, "Not Registered successfully, try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    @Override
    public void onClick(View v) {
        if (v == mSignUpBtn){
            signUp();
        }
    }
}
