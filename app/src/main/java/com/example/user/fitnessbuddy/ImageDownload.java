package com.example.user.fitnessbuddy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by user on 29.11.2016 г..
 */

public class ImageDownload extends AsyncTask<Void, Void, Bitmap>{

    private String url;
    private ImageView mapsView;

    public ImageDownload(String url, ImageView mapsView){
        this.url = url;
        this.mapsView = mapsView;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try{
            URL urlConnection = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlConnection
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result){
        super.onPostExecute(result);
        mapsView.setImageBitmap(result);
    }
}
