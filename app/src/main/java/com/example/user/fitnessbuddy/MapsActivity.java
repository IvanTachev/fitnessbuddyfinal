package com.example.user.fitnessbuddy;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PointOfInterest;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class MapsActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnPoiClickListener,
        View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;
    public static String someName;
    public static String someMood;
    private FloatingActionButton myLocFab;
    private TextView runTxtView;
    private Button startRunBtn;
    private Button endRunBtn;
    public FirebaseUser user;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference myRef;
    private GeoFire geoFire;
    private CameraPosition mCameraPosition;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    private static final int DEFAULT_ZOOM = 15;
    public LatLng startingLocation;
    public LatLng endLocation;
    public LatLng myLocation;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private SensorManager sensorManager;
    private Sensor sensor;
    boolean runs = false;
    public int steps;
    //    private String mLastUpdateTime;
    private String updateTime;
    private double distanceLongt;
    private double distanceLat;
    private String mLastUpdateTime;
    double[] Locations;
    boolean first;
    Location currentLocation;
    Location previousLocation;
    double totalDistance;
    double realDistance;
    Chronometer chronometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if (savedInstanceState != null) {
            mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        myRef = firebaseDatabase.getInstance().getReference("users" + "/" + user.getUid() + "/Runs");
        firebaseDatabase = FirebaseDatabase.getInstance();
        runTxtView = (TextView) findViewById(R.id.runTxtView);
        myLocFab = (FloatingActionButton) findViewById(R.id.myLocationButton);
        startRunBtn = (Button) findViewById(R.id.startRunBtn);
        endRunBtn = (Button) findViewById(R.id.endRunBtn);
        chronometer = (Chronometer) findViewById(R.id.chronometer);
        startRunBtn.setEnabled(false);
        endRunBtn.setEnabled(false);
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        startRunBtn.setOnClickListener(this);
        endRunBtn.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        runs = false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startRunBtn:
                nameSubmit();
                runTxtView.setText("Running");

                break;
            case R.id.endRunBtn:
                runningEnd();
                runTxtView.setText("Finished");

                break;
        }
    }

    public void runningStart() {
        endRunBtn.setEnabled(true);
        Running running = new Running();
        startingLocation = myLocation;
        running.startingPoint = startingLocation;
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(startingLocation.latitude, startingLocation.longitude))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title("Start destination"));
        startLocationUpdates();
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();





    }

    public void runningEnd() {
        Running running = new Running();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        myLocation = new LatLng(location.getLatitude(),
                location.getLongitude());
        endLocation = myLocation;
        running.finishPoint = myLocation;
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(endLocation.latitude, endLocation.longitude))
                .title("Final destination"));
        int td = running.totalDistance = 10;
        GeoFire geoFire = new GeoFire(myRef);
        realDistance = totalDistance*100000;

        String name = "Run" + someName;


        geoFire.setLocation(name + "Start", new GeoLocation(startingLocation.latitude, startingLocation.longitude));
        geoFire.setLocation(name + "Finish", new GeoLocation(endLocation.latitude, endLocation.longitude));
        chronometer.stop();
        long elapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();
        long time = elapsedMillis/1000;
        double avgSpeed = realDistance/time;
        myRef.child("name").setValue(name);
        myRef.child("totalDistance").setValue(realDistance);
        myRef.child("mood").setValue(someMood);
        myRef.child("averageSpeed").setValue(avgSpeed);

        stopLocationUpdates();




        finish();


    }

    public String nameSubmit() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.popup_window);
        final EditText nameET = (EditText) dialog.findViewById(R.id.runName);
        final EditText feelET = (EditText) dialog.findViewById(R.id.runFeel);
        Button submitBtn = (Button) dialog.findViewById(R.id.submitBtn);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancelBtn);

        dialog.show();

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                someName = nameET.getText().toString();
                someMood = feelET.getText().toString();
                runningStart();

                dialog.cancel();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        return someName;

    }



    @Override
    public void onPoiClick(PointOfInterest poi) {
        Toast.makeText(getApplicationContext(), "Clicked: " +
                        poi.name + "\nPlace ID:" + poi.placeId +
                        "\nLatitude:" + poi.latLng.latitude +
                        " Longitude:" + poi.latLng.longitude,
                Toast.LENGTH_SHORT).show();

        mMap.addMarker(new MarkerOptions()
                .position(poi.latLng)
                .title("Final Destination"));

    }

    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Toast.makeText(this, "Coordinates: " + mCurrentLocation, Toast.LENGTH_SHORT).show();

    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }



    @SuppressWarnings("MissingPermission")
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }

        if (mLocationPermissionGranted) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mCurrentLocation = null;
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this /* FragmentActivity */,
                        this /* OnConnectionFailedListener */)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    private void updateMarkers() {
        if (mMap == null) {
            return;
        }

        if (mLocationPermissionGranted) {
            @SuppressWarnings("MissingPermission")
            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                    .getCurrentPlace(mGoogleApiClient, null);
            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(@NonNull PlaceLikelihoodBuffer likelyPlaces) {
                    for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                        String attributions = (String) placeLikelihood.getPlace().getAttributions();
                        String snippet = (String) placeLikelihood.getPlace().getAddress();
                        if (attributions != null) {
                            snippet = snippet + "\n" + attributions;
                        }
                        mMap.addMarker(new MarkerOptions()
                                .position(placeLikelihood.getPlace().getLatLng())
                                .title((String) placeLikelihood.getPlace().getName())
                                .snippet(snippet));
                    }
                    likelyPlaces.release();
                }
            });
        } else {
            mMap.addMarker(new MarkerOptions()
                    .position(mDefaultLocation)
                    .title(getString(R.string.default_info_title))
                    .snippet(getString(R.string.default_info_snippet)));
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnPoiClickListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            return;
        }
        mMap.setMyLocationEnabled(true);
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            myLocation = new LatLng(location.getLatitude(),
                    location.getLongitude());
            Toast.makeText(MapsActivity.this, "This are thete coordinates " + myLocation.latitude + myLocation.longitude, Toast.LENGTH_LONG).show();
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.latitude, myLocation.longitude), 14.0f));
            mMap.setOnMyLocationButtonClickListener(this);
            startRunBtn.setEnabled(true);


        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        createLocationRequest();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Connection lost!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "No connection!", Toast.LENGTH_SHORT).show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        updateMarkers();
        mLastUpdateTime = java.text.DateFormat.getTimeInstance().format(new Date());
        previousLocation = currentLocation;
        currentLocation = location;
        if (currentLocation == null || previousLocation == null){
            first = true;
        }else {
            first = false;
        }
        updateUI(first, previousLocation, currentLocation);
    }

    public void stopLocationUpdates(){
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,this);
    }


    private void updateUI(boolean first, Location previousLocation, Location currentLocation){
        if (first == true){
            return;
        }else {
            PolylineOptions line = new PolylineOptions().add
                    (new LatLng(previousLocation.getLatitude(), previousLocation.getLongitude()),
                    new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude())).width(5).color(Color.BLUE);
            mMap.addPolyline(line);
            double sumX = (previousLocation.getLongitude() - currentLocation.getLongitude())*(previousLocation.getLongitude() - currentLocation.getLongitude());
            double sumY = (previousLocation.getLatitude() - currentLocation.getLatitude())*(previousLocation.getLatitude() - currentLocation.getLatitude());
            double distance = Math.sqrt(sumX+sumY);
            totalDistance = totalDistance + distance;
            Log.d("coordinates", String.valueOf(totalDistance));
        }

        Log.d("coordinates", String.valueOf(totalDistance));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mCurrentLocation);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }



}