package com.example.user.fitnessbuddy;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by user on 13.12.2016 г..
 */

public class ExerciseHolder extends RecyclerView.ViewHolder {
    private final TextView nameField;
    private final TextView descriptionField;

    public ExerciseHolder(View itemView) {
        super(itemView);
        nameField = (TextView) itemView.findViewById(R.id.exerciseNameShow);
        descriptionField = (TextView) itemView.findViewById(R.id.exerciseDescriptionShow);
    }

    public void setName(String name){
        nameField.setText(name);
    }
    public void setDescription(String text){
        descriptionField.setText(text);
    }


}
