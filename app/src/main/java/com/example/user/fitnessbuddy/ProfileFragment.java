package com.example.user.fitnessbuddy;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_OK;

//import java.net.URI;


public class ProfileFragment extends Fragment implements View.OnClickListener {
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private ImageView profilePicture;
    private DatabaseReference myRef;
    private Button profileSettings;
    private Button signOutBtn;
    private TextView userName;
    private TextView userBio;
    private FirebaseUser user;
    private static final int GALLERY_INTENT = 2;
    private static final int CAMERA_REQUEST_CODE = 1;
    private StorageReference myStorage;



    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.recent_activities);
//Initialising Database items
        profilePicture = (ImageView) v.findViewById(R.id.profile_picture);
        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        String userId = user.getUid();
        myRef = firebaseDatabase.getInstance().getReference("users");
        myStorage = FirebaseStorage.getInstance().getReference();
        final StorageReference oldPicture = myStorage.child(user.getUid()).child("profilePicture").child("profilePic.png");
        profilePictureSet();
        profileSettings  = (Button) v.findViewById(R.id.profileSettings);
        profileSettings.setOnClickListener(this);
        signOutBtn = (Button) v.findViewById(R.id.signOutBtn);
        signOutBtn.setOnClickListener(this);


        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StorageReference picture = oldPicture;
                changeProfilePicture(picture);
            }
        });

// Check if user is not logged in
        if (user == null){
            getActivity().finish();
            startActivity(new Intent(getActivity(), MainActivity.class));
        }
// End of check


        userName = (TextView) v.findViewById(R.id.user_profile_name);
        userBio = (TextView) v.findViewById(R.id.user_bio);
//Reading from database


        myRef.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null){
                    //Do nothing for now
                }else{
                    Account account = dataSnapshot.getValue(Account.class);
                    userName.setText(account.getName() + " " + account.getSurname());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        myRef.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Account account = new Account();
                if(dataSnapshot.getValue()==null){
                    userBio.setText("No biography entered");
                }else{
                    account = dataSnapshot.getValue(Account.class);
                    userBio.setText(account.getBiography());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        userBio.setText("Welcome " + user.getEmail());
        return v;
    }



    public void changeProfilePicture(final StorageReference picture){

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.profile_picture_pop_up);
        Button cameraBtn = (Button) dialog.findViewById(R.id.cameraBtn);
        Button galleryBtn = (Button) dialog.findViewById(R.id.galleryBtn);
        Button cancelBtn = (Button) dialog.findViewById(R.id.cancelBtn);
        dialog.show();

        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picture.delete();
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_INTENT);
                dialog.cancel();

            }
        });
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picture.delete();
                takePicture();
                dialog.cancel();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.profileSettings:
                startActivity(new Intent(getActivity(), ProfileSettingsActivity.class));
                break;
            case R.id.signOutBtn:
                firebaseAuth.signOut();
                getActivity().finish();
                startActivity(new Intent(getActivity(), MainActivity.class));
                break;

        }
    }

    public void profilePictureSet(){
        myStorage.child(user.getUid() + "/profilePicture/profilePic.png").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                new ImageDownload(uri.toString(),profilePicture).execute();
                Log.d("xxxx", uri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("xxxx", String.valueOf(e));

            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_INTENT && resultCode == RESULT_OK){

            Uri uri = data.getData();
            StorageReference filePath = myStorage.child(user.getUid()).child("profilePicture").child("profilePic.png");
            filePath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    profilePictureSet();
                    Toast.makeText(getActivity(), "Upload done", Toast.LENGTH_LONG).show();
                }
            });
        }
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle extra = data.getExtras();
                Bitmap bitmap = (Bitmap)data.getExtras().get("data");
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] dataBAOS = baos.toByteArray();
                //set image to profilePicture
                profilePicture.setImageBitmap(bitmap);
                //upload to Firebase

                StorageReference imageRef = myStorage.child(user.getUid()).child("profilePicture").child("profilePic.png");
                UploadTask uploadTask = imageRef.putBytes(dataBAOS);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(),"Sending failed", Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        profilePictureSet();
                    }
                });
            }
        }
    }

    private void takePicture(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }
}







