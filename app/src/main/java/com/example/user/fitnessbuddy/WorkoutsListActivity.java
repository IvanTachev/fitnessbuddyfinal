package com.example.user.fitnessbuddy;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class WorkoutsListActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseRecyclerAdapter<WeightsExercise, ExerciseHolder> myAdapter;
    DatabaseReference myRef;
    Button addNewExercise;
    public String name;
    public String description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts_list);
        RecyclerView exercisesList = (RecyclerView) findViewById(R.id.listExercises);
        myRef = FirebaseDatabase.getInstance().getReference("Exercises");
        exercisesList.setHasFixedSize(true);
        exercisesList.setLayoutManager(new LinearLayoutManager(this));
        addNewExercise = (Button) findViewById(R.id.exerciseDbAdd);
        addNewExercise.setOnClickListener(this);

        myAdapter = new FirebaseRecyclerAdapter<WeightsExercise, ExerciseHolder>(
                WeightsExercise.class,
                R.layout.row,
                ExerciseHolder.class,
                myRef) {
            @Override
            protected void populateViewHolder(ExerciseHolder viewHolder, WeightsExercise model, int position) {
                viewHolder.setName(model.getName());
                viewHolder.setDescription(model.getDescription());
            }
        };
        exercisesList.setAdapter(myAdapter);


    }



    public String newExerciseAdd(){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.add_exercise_popup);
        final EditText newExerciseName = (EditText)dialog.findViewById(R.id.newExerciseName);
        final EditText newExerciseDescription = (EditText)dialog.findViewById(R.id.newExerciseDescription);

        Button submitExercise = (Button) dialog.findViewById(R.id.submitExerciseBtn);
        Button cancelBtn = (Button) dialog.findViewById(R.id.cancelExerciseBtn);
        dialog.show();

        submitExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = newExerciseName.getText().toString();
                description = newExerciseDescription.getText().toString();
                WeightsExercise newExercise = new WeightsExercise(name, description);
                myRef.child(name).setValue(newExercise);
                Toast.makeText(WorkoutsListActivity.this, "Exercise successfully created", Toast.LENGTH_SHORT).show();
                dialog.cancel();

            }
        });


        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        return name;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.exerciseDbAdd:
                newExerciseAdd();
                break;
        }
    }
}