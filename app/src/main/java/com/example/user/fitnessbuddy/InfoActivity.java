package com.example.user.fitnessbuddy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class InfoActivity extends AppCompatActivity implements View.OnClickListener{

    EditText editName;
    EditText editSurname;
    EditText editHeight;
    EditText editWeight;
    EditText editBiography;


    Button saveBtn;

    DatabaseReference databaseRef;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_info);

        editName = (EditText) findViewById(R.id.nameGet);
        editSurname = (EditText) findViewById(R.id.surnameGet);
        editHeight = (EditText) findViewById(R.id.heightGet);
        editWeight = (EditText) findViewById(R.id.weightGet);
        editBiography = (EditText) findViewById(R.id.bioGet);
        saveBtn = (Button) findViewById(R.id.saveInfo);



        firebaseAuth = FirebaseAuth.getInstance();
        databaseRef = FirebaseDatabase.getInstance().getReference();
        final FirebaseUser user = firebaseAuth.getCurrentUser();


        saveBtn.setOnClickListener(this);
    }


    private void saveInfo(){
        if(editName.length()>0
                && editSurname.length()>0
                && editWeight.length()>0
                && editHeight.length()>0){
        String name = editName.getText().toString().trim();
        String surname = editSurname.getText().toString().trim();
        String xheight = editHeight.getText().toString().trim();
        int finalHeight = Integer.parseInt(xheight);
        String weight = editWeight.getText().toString().trim();
        int finalWeight = Integer.parseInt(weight);
        boolean loggedBfr = true;
        String biography = editBiography.getText().toString();


        Account account = new Account(name, surname, finalHeight, finalWeight, loggedBfr, biography);

        FirebaseUser user = firebaseAuth.getCurrentUser();
        databaseRef.child("users").child(user.getUid()).setValue(account);
        Toast.makeText(this, "Info saved", Toast.LENGTH_SHORT).show();
        finish();
        startActivity(new Intent(this, MainActivity.class));
        }else{
            Toast.makeText(InfoActivity.this, "Fill the form first!", Toast.LENGTH_SHORT).show();
        }


    }




    @Override
    public void onClick(View v) {

        if (v == saveBtn){
            saveInfo();
        }

    }
}
