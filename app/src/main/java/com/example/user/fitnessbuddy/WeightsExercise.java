package com.example.user.fitnessbuddy;

/**
 * Created by user on 10.12.2016 г..
 */

public class WeightsExercise {
    String name;
    String description;


    public WeightsExercise(){

    }

    public WeightsExercise(String name, String description){
        this.name = name;
        this.description = description;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}