package com.example.user.fitnessbuddy;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by user on 6.12.2016 г..
 */

public class DistanceCalculate extends AsyncTask<Object, Object, Location[]> {
    private Location currentLocation;
    public Location[] allLocations = new Location[20000];


    public DistanceCalculate(Location currentLocation){
        this.currentLocation = currentLocation;
    }


    @Override
    protected Location[] doInBackground(Object... params) {
        Location location = new Location(currentLocation);
        for (int j = 1;j<=20000;j++){
            allLocations[j-1]=location;
        }
        return allLocations;
    }




}