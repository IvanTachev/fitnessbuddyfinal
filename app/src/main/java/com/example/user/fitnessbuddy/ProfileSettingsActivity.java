package com.example.user.fitnessbuddy;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ProfileSettingsActivity extends AppCompatActivity implements View.OnClickListener{
    EditText nameNew;
    EditText surnameNew;
    EditText biographyNew;
    EditText weightNew;
    Button nameChangeBtn;
    Button surnameChangeBtn;
    Button biographyChangeBtn;
    Button weightChangeBtn;
    Button deleteAcc;

    DatabaseReference myRef;
    FirebaseAuth firebaseAuth;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);

        nameNew = (EditText) findViewById(R.id.nameNew);
        surnameNew = (EditText) findViewById(R.id.surnameNew);
        biographyNew = (EditText) findViewById(R.id.biographyNew);
        weightNew = (EditText) findViewById(R.id.weightNew);
        nameChangeBtn = (Button) findViewById(R.id.nameChange);
        surnameChangeBtn = (Button) findViewById(R.id.surnameChange);
        biographyChangeBtn = (Button) findViewById(R.id.biographyChange);
        weightChangeBtn = (Button) findViewById(R.id.weightChange);
        deleteAcc = (Button) findViewById(R.id.deleteAcc);

        firebaseAuth = FirebaseAuth.getInstance();
        final  FirebaseUser user = firebaseAuth.getCurrentUser();
        myRef = FirebaseDatabase.getInstance().getReference("users");

        nameChangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null){
                    if(nameNew.length()>0) {
                        String newName = nameNew.getText().toString();
                        myRef.child(user.getUid()).child("name").setValue(newName);
                        nameNew.setText("");
                        nameNew.setEnabled(false);
                        Toast.makeText(getApplication(), "Name successfully changed!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(ProfileSettingsActivity.this,"Enter text first", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        surnameChangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null){
                    if(surnameNew.length()>0) {

                        String newSurname = surnameNew.getText().toString();
                        myRef.child(user.getUid()).child("surname").setValue(newSurname);
                        surnameNew.setText("");
                        surnameNew.setEnabled(false);
                        Toast.makeText(getApplication(), "Surname successfully changed!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(ProfileSettingsActivity.this,"Enter text first", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        biographyChangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null) {
                    if (biographyNew.length() > 0) {

                        String newBiography = biographyNew.getText().toString();
                        myRef.child(user.getUid()).child("biography").setValue(newBiography);
                        biographyNew.setText("");
                        biographyNew.setEnabled(false);
                        Toast.makeText(getApplication(), "Biography successfully changed!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProfileSettingsActivity.this, "Enter text first", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        weightChangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null){
                    if (weightNew.length() > 0) {

                        int newWeight = Integer.parseInt(weightNew.getText().toString());
                        myRef.child(user.getUid()).child("finalWeight").setValue(newWeight);
                        weightNew.setText("");
                        weightNew.setEnabled(false);
                        Toast.makeText(getApplication(), "Weight successfully changed!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(ProfileSettingsActivity.this, "Enter text first", Toast.LENGTH_SHORT).show();

                    }
                }

            }
        });
        deleteAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null){
                    user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            myRef.child(user.getUid()).removeValue();
                            finish();
                            startActivity(new Intent(getApplication(), MainActivity.class));
                        }
                    });
                }


            }
        });

    }

    @Override
    public void onClick(View v) {

        /*switch (v.getId()){
            case R.id.nameChange:

                break;
            case R.id.surnameChange:

                break;
            case R.id.biographyChange:

                break;
            case R.id.weightChange:
                break;
            case R.id.deleteAcc:
                break;
        }*/
    }
}
