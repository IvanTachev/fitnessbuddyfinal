package com.example.user.fitnessbuddy;

/**
 * Created by user on 7.11.2016 г..
 */

public class Account {

    public String name;
    public String surname;
    public int finalHeight;
    public int finalWeight;
    public Boolean loggedBfr;
    public String biography;


    public Account() {

    }

    public Account(String name, String surname, int finalHeight, int finalWeight, boolean loggedBfr, String biography) {
        this.name = name;
        this.surname = surname;
        this.finalHeight = finalHeight;
        this.finalWeight = finalWeight;
        this.loggedBfr = loggedBfr;
        this.biography = biography;
    }


    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getSurname() {

        return surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public int getFinalHeight() {
        return finalHeight;
    }

    public void setFinalHeight(int finalHeight) {
        this.finalHeight = finalHeight;
    }

    public int getFinalWeight() {
        return finalWeight;
    }

    public void setFinalWeight(int finalWeight) {
        this.finalWeight = finalWeight;
    }

    public Boolean getLoggedBfr() {
        return loggedBfr;
    }

    public void setLoggedBfr(Boolean loggedBfr) {
        this.loggedBfr = loggedBfr;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

}

