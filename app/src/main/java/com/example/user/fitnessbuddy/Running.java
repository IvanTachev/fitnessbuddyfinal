package com.example.user.fitnessbuddy;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.Calendar;

/**
 * Created by user on 29.11.2016 г..
 */

public class Running {
    String name;
    LatLng startingPoint;
    LatLng finishPoint;
    int totalDistance;
    int averageSpeed;
    String mood;

    public Running(){

    }

    public Running(String name, LatLng startingPoint, LatLng finishPoint, int totalDistance, String mood, int averageSpeed){
        this.name = name;
        this.startingPoint = startingPoint;
        this.finishPoint = finishPoint;
        this.totalDistance = totalDistance;
        this.mood = mood;
        this.averageSpeed = averageSpeed;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getStartingPoint() {
        return startingPoint;
    }

    public void setStartingPoint(LatLng startingPoint) {
        this.startingPoint = startingPoint;
    }

    public LatLng getFinishPoint() {
        return finishPoint;
    }

    public void setFinishPoint(LatLng finishPoint) {
        this.finishPoint = finishPoint;
    }

    public int getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(int totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getMood() {
        return mood;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public int getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(int averageSpeed) {
        this.averageSpeed = averageSpeed;
    }
}


